extern crate gcc;
extern crate pkg_config;

fn main() {
    let mingw = std::env::var("TARGET").unwrap_or(String::new()).contains("mingw");

    let libs = pkg_config::Config::new().statik(mingw).cargo_metadata(false).probe("libavformat libavcodec libswscale libavutil").unwrap();

    for val in libs.link_paths.iter() {
        println!("cargo:rustc-link-search=native={}", val.display());
    }

    for val in libs.framework_paths.iter() {
        println!("cargo:rustc-link-search=framework={}", val.display());
    }

    for _val in libs.include_paths.iter() {
    }

    for i in 0..(libs.libs.len()) {
        if libs.libs[(i+1)..].iter().any(|s| { *s == libs.libs[i] }) {
            continue;
        }
        let s = if mingw { "static=" } else { "" };
        println!("cargo:rustc-link-lib={}{}", s, libs.libs[i]);
    }

    for val in libs.frameworks.iter() {
        println!("cargo:rustc-link-lib=framework={}", val);
    }

    gcc::Build::new()
        .file("src/encoder.c")
        .compile("libencoder.a");
}
