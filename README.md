# rechat-record

Download and record Twitch chat replay into MP4 video.

* Automatically downloads chat logs
* Rendering done offscreen
* Faster than real-time

## Download

[rechat_recorder_win_v10.zip](/uploads/0970d2d6153d21e498c381e5fd8c6e01/rechat_recorder_win_v10.zip)

**NB: after unpacking, you will need to download [phantomjs-2.1.1-windows.zip](https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-windows.zip) and unpack that inside the directory (next to the `bin`, `res` and `src` folders).**

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
